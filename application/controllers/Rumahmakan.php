<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class rumahmakan extends CI_Controller{
    function __construct() {
        parent::__construct();
        // error_reporting(0);
        $this->load->model('rumah_makan_model');

    }


	function index(){
        $config=array();
        $config['site_url'] = site_url('rumahmakan'); //merupakan alamat url menuju class/function pada controler yang berisi pagination
        $data['data'] = $this->rumah_makan_model->tampil($config);
		$this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('rumahmakan/content', $data);
        $this->load->view('footer');
    }

	public function add(){
        $data['data'] = $this->rumah_makan_model->kecamatan();
		    $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('rumahmakan/form',$data);
        $this->load->view('footer');
	}

    public function proses_tambah(){
        $this->load->library('upload');
        // $filename = $this->input->post('userfile');
        $config['upload_path'] = "./files/rumahmakan/";
        $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
        $config['overwrite'] = "true";
        $config['max_size'] = "90000000";
    		$config['max_width']="10000";
    		$config['max_height']="10000";
            // $config['file_name'] = $this->input->post('id_rumah_makan');
    		$config['file_name'] = ''.date('YmdHis');
        $this->upload->initialize($config);

        if(!$this->upload->do_upload()){
            echo  $this->upload->display_errors();
        }else{
            $dat = $this->upload->data();
            $data['id_rumah_makan'] = $this->input->post('id_rumah_makan');
            $data['nama_rumah_makan'] = $this->input->post('nama_rumah_makan');
            $data['alamat_rumah_makan'] = $this->input->post('alamat_rumah_makan');
            $data['nomor_hp_rumah_makan'] = $this->input->post('nomor_hp_rumah_makan');
            $data['id_kecamatan'] = $this->input->post('id_kecamatan');
            $data['deskripsi_rumah_makan'] = $this->input->post('deskripsi_rumah_makan');
            $data['latitude'] = $this->input->post('latitude');
            $data['longitude'] = $this->input->post('longitude');
            // $data['gambar'] = $this->input->post('file_name');
			$data['gambar'] = $dat['file_name'];

		$this->rumah_makan_model->tambah($data);

		redirect(base_url('/rumahmakan')."");

        }
    }

    public function proses_hapus(){
        $data['id_rumah_makan'] = $this->input->get('id_rumah_makan');
        if($data['id_rumah_makan'] != ""){
            $this->rumah_makan_model->hapus($data);
        }
        redirect(base_url('/rumahmakan')."");
    }

    public function edit(){
        $data['k'] = $this->rumah_makan_model->kecamatan();
        $id_rumah_makan = $this->input->get('id_rumah_makan');
        $data['entry'] = $this->rumah_makan_model->get($id_rumah_makan);
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect(base_url('/rumahmakan')."");
        }else{
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('sidebar');
            $this->load->view('rumahmakan/form_edit',$data);
            $this->load->view('footer');
        }
    }

    public function proses_edit(){

        if($_FILES['userfile']['name']==""){
          $data['id_rumah_makan'] = $this->input->post('id_rumah_makan');
          $data['nama_rumah_makan'] = $this->input->post('nama_rumah_makan');
          $data['alamat_rumah_makan'] = $this->input->post('alamat_rumah_makan');
          $data['nomor_hp_rumah_makan'] = $this->input->post('nomor_hp_rumah_makan');
          $data['id_kecamatan'] = $this->input->post('id_kecamatan');
          $data['deskripsi_rumah_makan'] = $this->input->post('deskripsi_rumah_makan');
          $data['latitude'] = $this->input->post('latitude');
          $data['longitude'] = $this->input->post('longitude');

          $this->rumah_makan_model->update($data);
          redirect(base_url('/rumahmakan')."");

        }else{
          $this->load->library('upload');
          // $filename = $this->input->post('userfile');
          $config['upload_path'] = "./files/rumahmakan/";
          $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
          $config['overwrite'] = "true";
          $config['max_size'] = "90000000";
          $config['max_width']="10000";
          $config['max_height']="10000";
              // $config['file_name'] = $this->input->post('id_rumah_makan');
          $config['file_name'] = ''.date('YmdHis');
          $this->upload->initialize($config);

          if(!$this->upload->do_upload()){
              echo  $this->upload->display_errors();
          }else{
            $dat = $this->upload->data();
            $data['id_rumah_makan'] = $this->input->post('id_rumah_makan');
            $data['nama_rumah_makan'] = $this->input->post('nama_rumah_makan');
            $data['alamat_rumah_makan'] = $this->input->post('alamat_rumah_makan');
            $data['nomor_hp_rumah_makan'] = $this->input->post('nomor_hp_rumah_makan');
            $data['id_kecamatan'] = $this->input->post('id_kecamatan');
            $data['deskripsi_rumah_makan'] = $this->input->post('deskripsi_rumah_makan');
            $data['latitude'] = $this->input->post('latitude');
            $data['longitude'] = $this->input->post('longitude');
            // $data['gambar'] = $this->input->post('file_name');
            $data['gambar'] = $dat['file_name'];

            $this->rumah_makan_model->update($data);
            redirect(base_url('/rumahmakan')."");

          }
        }
      }

}
