<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class fasilitas extends CI_Controller{
    function __construct() {
        parent::__construct();
        // error_reporting(0);
        $this->load->model('fasilitas_model');
    }


	public function index(){
        $config=array();
        $config['site_url'] = site_url('fasilitas'); 
        $data['data'] = $this->fasilitas_model->tampil($config);
		$this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('fasilitas/content', $data);      
        $this->load->view('footer');
    }

	public function add()
	{
		$this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('fasilitas/form');      
        $this->load->view('footer');
	}

    public function proses_tambah(){
        $data['id_fasilitas'] = $this->input->post('id_fasilitas');
        $data['nama_fasilitas'] = $this->input->post('nama_fasilitas');

        $this->fasilitas_model->tambah($data);
        redirect(base_url('/Fasilitas')."");
    }

    public function proses_hapus(){
        $data['id_fasilitas'] = $this->input->get('id_fasilitas');
        if($data['id_fasilitas'] != "") {
            $this->fasilitas_model->hapus($data);
        }
        redirect(base_url('/Fasilitas')."");
    }

    public function edit(){
        $id_fasilitas = $this->input->get('id_fasilitas');
        $data['entry'] = $this->fasilitas_model->get($id_fasilitas);
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect(base_url('/fasilitas')."");
        }else{
            $data['entry'] = $data['entry'][0];
            $this->load->view('fasilitas/header');
            $this->load->view('fasilitas/sidebar');
            $this->load->view('fasilitas/form_edit', $data);      
            $this->load->view('fasilitas/footer');
        }
    }

    public function proses_edit(){
        $data['id_fasilitas'] = $this->input->post('id_fasilitas');
        $data['nama_fasilitas'] = $this->input->post('nama_fasilitas');

        $this->fasilitas_model->update($data);
        redirect(base_url('/fasilitas')."");
    }
}













