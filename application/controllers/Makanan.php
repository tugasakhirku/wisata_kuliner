<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class makanan extends CI_Controller{
    function __construct() {
        parent::__construct();
        // error_reporting(0);
        $this->load->model('makanan_model');
    }


	 public function index(){
        $config=array();
        $config['site_url'] = site_url('makanan');
        $data['data'] = $this->makanan_model->tampil($config);
		    $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('makanan/content', $data);
        $this->load->view('footer');
    }

	 public function add(){
		    $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('makanan/form');
        $this->load->view('footer');
	}

    public function proses_tambah(){
        $this->load->library('upload');
        // $filename = $this->input->post('userfile');
        $config['upload_path'] = "./files/makanan/";
        $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
        $config['overwrite'] = "true";
        $config['max_size'] = "90000000";
    		$config['max_width']="10000";
    		$config['max_height']="10000";
            // $config['file_name'] = $this->input->post('id_rumah_makan');
    		$config['file_name'] = ''.date('YmdHis');
        $this->upload->initialize($config);

        if(!$this->upload->do_upload()){
            echo  $this->upload->display_errors();
        }else{
      			$dat = $this->upload->data();
      			$data['id_makanan'] = $this->input->post('id_makanan');
      			$data['nama_makanan'] = $this->input->post('nama_makanan');
      			$data['harga_makanan'] = $this->input->post('harga_makanan');
      			$data['deskripsi'] = $this->input->post('deskripsi');
      			$data['gambar'] = $dat['file_name'];

            $this->makanan_model->tambah($data);
            redirect(base_url('/Makanan')."");
		}
    }

    public function proses_hapus(){
        $data['id_makanan'] = $this->input->get('id_makanan');
        if($data['id_makanan'] != ""){
            $this->makanan_model->hapus($data);
        }
        redirect(base_url('/Makanan')."");
    }

    public function edit(){
        $id_makanan = $this->input->get('id_makanan');
        $data['entry'] = $this->makanan_model->get($id_makanan);
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect(base_url('/Makanan')."");
        }else{
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('sidebar');
            $this->load->view('makanan/form_edit', $data);
            $this->load->view('footer');
        }
    }

    public function proses_edit(){
        if($_FILES['userfile']['name']==""){
          $data['id_makanan'] = $this->input->post('id_makanan');
          $data['nama_makanan'] = $this->input->post('nama_makanan');
          $data['harga_makanan'] = $this->input->post('harga_makanan');
          $data['deskripsi'] = $this->input->post('deskripsi');

          $this->makanan_model->update($data);
          redirect(base_url('/Makanan')."");
        }else{$this->load->library('upload');
            // $filename = $this->input->post('userfile');
            $config['upload_path'] = "./files/makanan/";
            $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
            $config['overwrite'] = "true";
            $config['max_size'] = "90000000";
        		$config['max_width']="10000";
        		$config['max_height']="10000";
                // $config['file_name'] = $this->input->post('id_rumah_makan');
        		$config['file_name'] = ''.date('YmdHis');
            $this->upload->initialize($config);

            if(!$this->upload->do_upload()){
                echo  $this->upload->display_errors();
            }else{
          			$dat = $this->upload->data();
          			$data['id_makanan'] = $this->input->post('id_makanan');
          			$data['nama_makanan'] = $this->input->post('nama_makanan');
          			$data['harga_makanan'] = $this->input->post('harga_makanan');
          			$data['deskripsi'] = $this->input->post('deskripsi');
          			$data['gambar'] = $dat['file_name'];

                $this->makanan_model->update($data);
                redirect(base_url('/Makanan')."");
    		        }
              }
          }

}
