<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class minuman extends CI_Controller{
    function __construct() {
        parent::__construct();
        // error_reporting(0);
        $this->load->model('minuman_model');
    }


	 function index(){
        $config=array();
        $config['site_url'] = site_url('minuman');
        $data['data'] = $this->minuman_model->tampil($config);
		    $this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('minuman/content', $data);
        $this->load->view('footer');
    }

	public function add()
	{
		$this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('minuman/form');
        $this->load->view('footer');
	}

    public function proses_tambah(){
        $this->load->library('upload');
        // $filename = $this->input->post('userfile');
        $config['upload_path'] = "./files/minuman/";
        $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
        $config['overwrite'] = "true";
        $config['max_size'] = "90000000";
		$config['max_width']="10000";
		$config['max_height']="10000";
        // $config['file_name'] = $this->input->post('id_rumah_makan');
		$config['file_name'] = ''.date('YmdHis');
        $this->upload->initialize($config);

        if(!$this->upload->do_upload()){
            echo  $this->upload->display_errors();
        }else{
			$dat = $this->upload->data();
			$data['id_minuman'] = $this->input->post('id_minuman');
			$data['nama_minuman'] = $this->input->post('nama_minuman');
			$data['harga_minuman'] = $this->input->post('harga_minuman');
			$data['deskripsi'] = $this->input->post('deskripsi');
			$data['gambar'] = $dat['file_name'];

        $this->minuman_model->tambah($data);
        redirect(base_url('/minuman')."");
		}
	}

    public function proses_hapus(){
        $data['id_minuman'] = $this->input->get('id_minuman');
        if($data['id_minuman'] != ""){
            $this->minuman_model->hapus($data);
        }
        redirect(base_url('/minuman')."");
    }

    public function edit(){
        $id_minuman = $this->input->get('id_minuman');
        $data['entry'] = $this->minuman_model->get($id_minuman);
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect(base_url('/minuman')."");
        }else{
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('sidebar');
            $this->load->view('minuman/form_edit',$data);
            $this->load->view('footer');
        }
    }

    public function proses_edit(){
        // echo $_FILES['userfile']['name'];

        if($_FILES['userfile']['name']==""){
            $data['id_minuman'] = $this->input->post('id_minuman');
            $data['nama_minuman'] = $this->input->post('nama_minuman');
            $data['harga_minuman'] = $this->input->post('harga_minuman');
            $data['deskripsi'] = $this->input->post('deskripsi');

            $this->minuman_model->update($data);
            redirect(base_url('/minuman')."");

        }else{
            $this->load->library('upload');
            // $filename = $this->input->post('userfile');
            $config['upload_path'] = "./files/minuman/";
            $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
            $config['overwrite'] = "true";
            $config['max_size'] = "90000000";
            $config['max_width']="10000";
            $config['max_height']="10000";
            // $config['file_name'] = $this->input->post('id_rumah_makan');
            $config['file_name'] = ''.date('YmdHis');
            $this->upload->initialize($config);

            if(!$this->upload->do_upload()){
                echo  $this->upload->display_errors();
            }else{
                $dat = $this->upload->data();
                $data['id_minuman'] = $this->input->post('id_minuman');
                $data['nama_minuman'] = $this->input->post('nama_minuman');
                $data['harga_minuman'] = $this->input->post('harga_minuman');
                $data['deskripsi'] = $this->input->post('deskripsi');
                $data['gambar'] = $dat['file_name'];

            $this->minuman_model->update($data);
            redirect(base_url('/minuman')."");

            }
        }
        // $this->minuman_model->update($data);
        // redirect(base_url('/minuman')."");
    }


}
