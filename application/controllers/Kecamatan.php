<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kecamatan extends CI_Controller{
    function __construct() {
        parent::__construct();
        // error_reporting(0);
        $this->load->model('kecamatan_model');
    }


	public function index(){
        $config=array();
        $config['site_url'] = site_url('kecamatan'); 
        $data['data'] = $this->kecamatan_model->tampil($config);
		$this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('kecamatan/content', $data);      
        $this->load->view('footer');
    }

	public function add()
	{
		$this->load->view('header');
        $this->load->view('sidebar');
        $this->load->view('kecamatan/form');      
        $this->load->view('footer');
	}

    public function proses_tambah(){
        $data['id_kecamatan'] = $this->input->post('id_kecamatan');
        $data['nama_kecamatan'] = $this->input->post('nama_kecamatan');

        $this->kecamatan_model->tambah($data);
        redirect(base_url('/kecamatan')."");
    }

    public function proses_hapus(){
        $data['id_kecamatan'] = $this->input->get('id_kecamatan');
        if($data['id_kecamatan'] != "") {
            $this->kecamatan_model->hapus($data);
        }
        redirect(base_url('/kecamatan')."");
    }

    public function edit(){
        $id_kecamatan = $this->input->get('id_kecamatan');
        $data['entry'] = $this->kecamatan_model->get($id_kecamatan);
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect(base_url('/kecamatan')."");
        }else{
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('sidebar');
            $this->load->view('kecamatan/form_edit', $data);      
            $this->load->view('footer');
        }
    }

    public function proses_edit(){
        $data['id_kecamatan'] = $this->input->post('id_kecamatan');
        $data['nama_kecamatan'] = $this->input->post('nama_kecamatan');

        $this->kecamatan_model->update($data);
        redirect(base_url('/kecamatan')."");
    }
}













