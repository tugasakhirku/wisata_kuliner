<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );


class makanan_model extends CI_Model
{
	
	public function __construct(){
		parent::__construct();
	}

	public function tambah($data){
		$this->db->insert('makanan', $data);
	}

	public function tampil(){
		$query = $this->db->get('makanan');
		if($query->num_rows() > 0){
			foreach ($query->result () as $row){
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}	
	
	public function hapus($data){
		$this->db->delete('makanan', array('id_makanan' =>$data['id_makanan']));
	}

	public function get($id_makanan){
		$this->db->where('id_makanan', $id_makanan);
		$query = $this->db->get('makanan', 1);
		return $query->result();
	}

	public function update($data){
		$this->db->update('makanan', $data, array('id_makanan'=>$data['id_makanan']));
	}


}