<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );


class rumah_makan_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function tambah($data){
		$this->db->insert('rumah_makan_tbl', $data);
	}

	public function kecamatan()
	{
		$query = $this->db->get('kecamatan');
		if($query->num_rows() > 0) {
			foreach($query->result () as $row){
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function tampil()
	{
		$this->db->select('*');
		$this->db->from('rumah_makan_tbl r');
		$this->db->join('kecamatan k', 'r.id_kecamatan=k.id_kecamatan', 'left');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			foreach ($query->result () as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function hapus($data){
		$this->db->delete('rumah_makan_tbl', array('id_rumah_makan' =>$data['id_rumah_makan']));
	}
	
	public function get($id_rumah_makan){
		$this->db->where('id_rumah_makan', $id_rumah_makan);
		$query = $this->db->get('rumah_makan_tbl', 1);
		return $query->result();
	}

	public function update($data){
		$this->db->update('rumah_makan_tbl', $data, array('id_rumah_makan'=>$data['id_rumah_makan']));
	}


}