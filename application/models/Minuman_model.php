<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );


class minuman_model extends CI_Model
{
	
	public function __construct(){
		parent::__construct();
	}

	public function tambah($data){
		$this->db->insert('minuman', $data);
	}

	public function tampil(){
		$query = $this->db->get('minuman');
		if($query->num_rows() > 0){
			foreach ($query->result () as $row){
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}	
	
	public function hapus($data){
		$this->db->delete('minuman', array('id_minuman' =>$data['id_minuman']));
	}

	public function get($id_minuman){
		$this->db->where('id_minuman', $id_minuman);
		$query = $this->db->get('minuman', 1);
		return $query->result();
	}

	public function update($data){
		$this->db->update('minuman', $data, array('id_minuman'=>$data['id_minuman']));
		
	}


}