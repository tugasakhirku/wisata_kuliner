<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );


class fasilitas_model extends CI_Model
{
	
	public function __construct(){
		parent::__construct();
	}

	public function tambah($data){
		$this->db->insert('fasilitas', $data);
	}

	public function tampil(){
		$query = $this->db->get('fasilitas');
		if($query->num_rows() > 0){
			foreach ($query->result () as $row){
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}	
	
	public function hapus($data){
		$this->db->delete('fasilitas', array('id_fasilitas' =>$data['id_fasilitas']));
	}

	public function get($id_fasilitas){
		$this->db->where('id_fasilitas', $id_fasilitas);
		$query = $this->db->get('fasilitas', 1);
		return $query->result();
	}

	public function update($data){
		$this->db->update('fasilitas', $data, array('id_fasilitas'=>$data['id_fasilitas']));
	}


}