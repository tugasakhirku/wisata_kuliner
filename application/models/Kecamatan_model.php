<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );


class kecamatan_model extends CI_Model
{
	
	public function __construct(){
		parent::__construct();
	}

	public function tambah($data){
		$this->db->insert('kecamatan', $data);
	}

	public function tampil(){
		$query = $this->db->get('kecamatan');
		if($query->num_rows() > 0){
			foreach ($query->result () as $row){
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}	
	
	public function hapus($data){
		$this->db->delete('kecamatan', array('id_kecamatan' =>$data['id_kecamatan']));
	}

	public function get($id_kecamatan){
		$this->db->where('id_kecamatan', $id_kecamatan);
		$query = $this->db->get('kecamatan', 1);
		return $query->result();
	}

	public function update($data){
		$this->db->update('kecamatan', $data, array('id_kecamatan'=>$data['id_kecamatan']));
	}


}