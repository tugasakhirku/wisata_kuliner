<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Wisata Kuliner Kota Kendari</title>

    <!-- Bootstrap CSS -->        
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	
    <!-- bootstrap theme -->    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css">
	
    <!--external css-->
    <!-- font icon -->    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/elegant-icons-style.css">    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
	
    <!-- full calendar css-->    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/fullcalendar/fullcalendar/fullcalendar.css">	
	
    <!-- easy pie chart-->    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css">	
    
	<!-- owl carousel -->    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-jvectormap-1.2.2.css">	
	
	
    <!-- Custom styles -->	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/widgets.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style-responsive.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style-responsive.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/xcharts.min.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.4.min.css">	
	
    <!-- =======================================================
        Theme Name: NiceAdmin
        Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
        Author: BootstrapMade
        Author URL: https://bootstrapmade.com
    ======================================================= -->
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      <!--header -->
      <header class="header dark-bg">
           <!--  <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div> -->

            <!--logo start-->
            <a href="index.html" class="logo">Sistem Informasi <span class="lite">Kuliner Kota Kendari</span></a>
            <!--logo end-->

            <div class="nav search-row" id="top_menu">
                <!--  search form start -->
                <!-- <ul class="nav top-menu">                    
                    <li>
                        <form class="navbar-form">
                            <input class="form-control" placeholder="Search" type="text">
                        </form>
                    </li>                    
                </ul> -->
                <!--  search form end -->                
            </div>

            <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    

                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <span class="profile-ava">
                                <img alt="" src="img/avatar1_small.jpg">
                                <link alt="" href="<?php echo base_url(); ?>assets/img/icons/avatar1_small.jpg"> 
                            </span> -->
                            <span class="username">Admin</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="#"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_mail_alt"></i> My Inbox</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_clock_alt"></i> Timeline</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_chat_alt"></i> Chats</a>
                            </li>
                            <li>
                                <a href="login.html"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                            <li>
                                <a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
                            </li>
                            <li>
                                <a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
                            </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li class="active">
                      <a class="" >
                          <i class=""></i>
                          <span>Wisata Kuliner Kendari</span>
                      </a>
                  </li>
				  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="fa fa-home"></i>
                          <span>Rumah Makan</span>
                          <!-- <span class="menu-arrow arrow_carrot-right"></span> -->
                      </a>
                      <!-- <ul class="sub">
                          <li><a class="" href="form_component.html">Form Elements</a></li>                          
                          <li><a class="" href="form_validation.html">Form Validation</a></li>
                      </ul> -->
                  </li>       
                  <!-- <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_desktop"></i>
                          <span>UI Fitures</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><a class="" href="general.html">Elements</a></li>
                          <li><a class="" href="buttons.html">Buttons</a></li>
                          <li><a class="" href="grids.html">Grids</a></li>
                      </ul>
                  </li> -->
                  
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!--main content start-->
		<section id="main-content">
			<section class="wrapper">            
              <!--overview start-->
			  <div class="row">
				<div class="col-lg-12">
					<!-- <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3> -->
                    <!-- <h4 class="page-header"><i class=""></i> Dashboard</h3> -->

					<ol class="breadcrumb">
						<li><i class="page-header"></i><a href="indexis.html">Home</a></li>
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Profil</a></li> -->
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Peta</a></li> -->
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Daftar Tempat Kuliner</a></li> -->
						<li><i class="fa fa-home"></i>Rumah Makan</li>						  	
					</ol>
				</div>
			  </div>
              <!--end of overview start-->

              <!-- /.panel-heading -->
                <div class="panel-body">
                    
                <form class="navbar-form navbar-left" role="search">
                    <div class="btn-group">
                        <?php echo anchor('rumahmakan/add', 
                                '<button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Tambah Data Pegawai"><i class="fa fa-plus"></i> Tambah Rumah Makan</button>' );?>
                                &nbsp;
                                <?php echo anchor('Rumahmakan', 
                                '<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    
                    <!-- <button type="submit" class="btn btn-default" title="Cari Data" >Go</button> -->
                    <div class="btn-group">
                        <input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
                    </div>                    
                  
                </form>

                </div>
              <!-- /end of.panel-heading -->

              <!--tambahan panel table-->
    <div class="panel-body">
        <div class="dataTable_wrapper">
            <div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
				<thead>
				<tr>
					<th style="width:5%"><center>No</th>
					<th style="width:10%"><center>Nama</th>                
					<th style="width:15%"><center>Deskripsi</th>
					<th style="width:10%"><center>Kecamatan</th>
					<th style="width:10%"><center>Latitude</th>
					<th style="width:10%"><center>Longitude</th>
					<th style="width:10%"><center>Gambar</th>
					<th style="width:20%"><center>Pilihan</th>
				</tr>
				</thead>
				<tbody>
			   
					<tr class="gradeX" >
					
					</tr>
				
				</tbody>
				</table>
            </div>
            <!-- <div align="right"><?php echo $links?> </div> -->
        </div>
    </div>

              <!--end of tambahan panel table-->


			</section>
			
      <!--footer start -->
      <div class="center">
				<div class="credits">
                <!-- 
                    All the links in the footer should remain intact. 
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
                -->
					<a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Bisis Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
				</div>
			</div>
		</section>
      <!--main content end-->
  </section>
  <!-- end container section  -->

    <!-- javascripts -->    
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery.js"></script>	
    <script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery-ui-1.10.4.min.js"></script>	
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery-1.8.3.min.js"></script>	
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>	
	
    <!-- bootstrap -->    
    <script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>	
	
	<!-- nice scroll -->    
    <script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>	
    <script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>	
	
    <!-- charts scripts -->
    <script type ='text/javascript' src = "<?php echo base_url(); ?>assets/assets/jquery-knob/js/jquery.knob.js"></script>	
    <script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery.sparkline.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/owl.carousel.js"></script>
	
    <!-- jQuery full calendar -->    
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/assets/fullcalendar/fullcalendar/fullcalendar.js"></script>

    <!--script for this page only-->    
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/calendar-custom.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery.rateit.min.js"></script>
	
    <!-- custom select -->    
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery.customSelect.min.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/Chart.js"></script>
   
    <!--custome script for all page-->    	
    <script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/scripts.js"></script>
	
	<!-- custom script for this page-->    
    <script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/sparkline-chart.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/easy-pie-chart.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery-jvectormap-1.2.2.min.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery-jvectormap-world-mill-en.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/xcharts.min.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery.autosize.min.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery.placeholder.min.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/gdp-data.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/morris.min.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/sparklines.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/charts.js"></script>
	<script type ='text/javascript' src = "<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js"></script>
	
	
  <script>

      //knob
      $(function() {
        $(".knob").knob({
          'draw' : function () { 
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
          $("#owl-slider").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });
	  
	  /* ---------- Map ---------- */
	$(function(){
	  $('#map').vectorMap({
	    map: 'world_mill_en',
	    series: {
	      regions: [{
	        values: gdpData,
	        scale: ['#000', '#000'],
	        normalizeFunction: 'polynomial'
	      }]
	    },
		backgroundColor: '#eef3f7',
	    onLabelShow: function(e, el, code){
	      el.html(el.html()+' (GDP - '+gdpData[code]+')');
	    }
	  });
	});

  </script>

  </body>
</html>

