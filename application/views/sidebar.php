      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">                
                  <li class="active">
                      <a class="" >
                          <i class=""></i>
                          <span>Wisata Kuliner Kendari</span>
                      </a>
                  </li>
      				  <li class="sub-menu">
                            <a href="<?php echo site_url('minuman'); ?>" class="">
                                <i class=""></i>
                                <span>Minuman</span>
                                <!-- <span class="menu-arrow arrow_carrot-right"></span> -->
                            </a>
                </li>       
                <li class="sub-menu">
                            <a href="<?php echo site_url('rumahmakan'); ?>" class="">
                                <i class=""></i>
                                <span>Rumah Makan</span>
                                <!-- <span class="menu-arrow arrow_carrot-right"></span> -->
                            </a>
                            
                            <!-- <ul class="sub">
                                <li><a class="" href="form_component.html">Form Elements</a></li>                          
                                <li><a class="" href="form_validation.html">Form Validation</a></li>
                            </ul> -->
                </li>       
                <li class="sub-menu">
                            <a href="<?php echo site_url('makanan'); ?>" class="">
                                <i class=""></i>
                                <span>Makanan</span>
                                <!-- <span class="menu-arrow arrow_carrot-right"></span> -->
                            </a>
                            
                            <!-- <ul class="sub">
                                <li><a class="" href="form_component.html">Form Elements</a></li>                          
                                <li><a class="" href="form_validation.html">Form Validation</a></li>
                            </ul> -->
                </li>       
                <li class="sub-menu">
                            <a href="<?php echo site_url('fasilitas'); ?>" class="">
                                <i class=""></i>
                                <span>Fasilitas</span>
                                <!-- <span class="menu-arrow arrow_carrot-right"></span> -->
                            </a>
                            
                            <!-- <ul class="sub">
                                <li><a class="" href="form_component.html">Form Elements</a></li>                          
                                <li><a class="" href="form_validation.html">Form Validation</a></li>
                            </ul> -->
                </li>       
                <li class="sub-menu">
                            <a href="<?php echo site_url('kecamatan'); ?>" class="">
                                <i class=""></i>
                                <span>Kecamatan</span>
                                <!-- <span class="menu-arrow arrow_carrot-right"></span> -->
                            </a>
                            
                            <!-- <ul class="sub">
                                <li><a class="" href="form_component.html">Form Elements</a></li>                          
                                <li><a class="" href="form_validation.html">Form Validation</a></li>
                            </ul> -->
                </li>       
                        <!-- <li class="sub-menu">
                            <a href="javascript:;" class="">
                                <i class="icon_desktop"></i>
                                <span>UI Fitures</span>
                                <span class="menu-arrow arrow_carrot-right"></span>
                            </a>
                            <ul class="sub">
                                <li><a class="" href="general.html">Elements</a></li>
                                <li><a class="" href="buttons.html">Buttons</a></li>
                                <li><a class="" href="grids.html">Grids</a></li>
                            </ul>
                        </li> -->
                        
                        
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->