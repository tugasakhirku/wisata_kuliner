<!--main content start-->
		<section id="main-content">
			<section class="wrapper">            
              <!--overview start-->
			  <div class="row">
				<div class="col-lg-12">
					<!-- <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3> -->
                    <!-- <h4 class="page-header"><i class=""></i> Dashboard</h3> -->

					<ol class="breadcrumb">
						<li><i class="page-header"></i><a href="indexis.html">Home</a></li>
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Profil</a></li> -->
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Peta</a></li> -->
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Daftar Tempat Kuliner</a></li> -->
						<li><i class="fa fa-home"></i>Makanan</li>						  	
					</ol>
				</div>
			  </div>
              <!--end of overview start-->

              <!-- /.panel-heading -->
                <div class="panel-body">
                    
                

                </div>
              <!-- /end of.panel-heading -->

              <!--tambahan panel table-->
			<div class="box-body">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div id="header">
								<h1 id="logo">Tambah Makanan</h1>
								<hr>                        
							<div class="widget-body">
								<?php echo form_open_multipart('makanan/proses_tambah', 'class="form-horizontal", row-border'); ?>
									<div class="form-group">
										<label class="col-md-2 control-label">Id Makanan</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="id_makanan" placeholder="" maxlength="100"  readonly>
											</div>
									</div>
									
									<!-- <div class="form-group">
										<label class="col-md-2 control-label">Id Tempat Makan</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="id_barang" placeholder="" maxlength="100"  readonly>
											</div>
									</div> -->
									
									<div class="form-group">
										<label class="col-md-2 control-label">Nama Makanan</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="nama_makanan" placeholder="Masukkan Nama Makanan" maxlength="100" required>
											</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-2 control-label">Harga Makanan</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="harga_makanan" placeholder="Masukkan Harga" maxlength="100" required>
											</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-2 control-label">Deskripsi</label>
											<div class="col-md-6">
												<textarea class="form-control" rows="3" name="deskripsi" placeholder="Deskripsi Makanan" required></textarea>
											</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-2 control-label">Gambar</label>
											<div class="col-md-6">
												<input class="form-control" type="file" name="userfile" placeholder="Masukkan Nama Fasilitas" maxlength="100" required>
											</div>
									</div>
									
									
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<button type="submit" class="btn btn-info">Simpan</button>
											<button type="reset" class="btn btn-danger" title="kembali">Reset</button>
												<?php echo anchor('/makanan', '<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="kembali ke file index ke tampilan jenis barang">Kembali</button>');  ?>
										</div>
									</div>
								<?php echo form_close(); ?>

							</div>
						</div>
					</div>
        
				</div>
		  
		  
			</div>
        <!-- /.box-body -->
	
	

              <!--end of tambahan panel table-->

			</section>