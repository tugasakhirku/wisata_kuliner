<!--main content start-->
		<section id="main-content">
			<section class="wrapper">            
              <!--overview start-->
			  <div class="row">
				<div class="col-lg-12">
					<!-- <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3> -->
                    <!-- <h4 class="page-header"><i class=""></i> Dashboard</h3> -->

					<ol class="breadcrumb">
						<li><i class="page-header"></i><a href="indexis.html">Home</a></li>
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Profil</a></li> -->
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Peta</a></li> -->
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Daftar Tempat Kuliner</a></li> -->
						<li><i class="fa fa-home"></i>Fasilitas</li>						  	
					</ol>
				</div>
			  </div>
              <!--end of overview start-->

              <!-- /.panel-heading -->
                <div class="panel-body">
                    
                

                </div>
              <!-- /end of.panel-heading -->

              <!--tambahan panel table-->
			<div class="box-body">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div id="header">
								<h1 id="logo">Edit Fasilitas</h1>
								<hr>                        
							<div class="widget-body">
								<?php echo form_open('fasilitas/proses_edit','class="form-horizontal", row-border'); ?>
									<div class="form-group">
										<label class="col-md-2 control-label">Id Fasilitas</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="id_fasilitas" value="<?php echo $entry->id_fasilitas ?>"  readonly>
											</div>
									</div>
									
									<!-- <div class="form-group">
										<label class="col-md-2 control-label">Id Tempat Makan</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="id_barang" placeholder="" maxlength="100"  readonly>
											</div>
									</div> -->
									
									<div class="form-group">
										<label class="col-md-2 control-label">Nama Fasilitas</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="nama_fasilitas" placeholder="Masukkan Nama Fasilitas" maxlength="100" value="<?php echo $entry->nama_fasilitas ?>">
											</div>
									</div>
									
									
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<button type="submit" class="btn btn-info">Simpan</button>
												<?php echo anchor('/fasilitas', '<button type="submit" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="kembali ke file index Fasilitas">Kembali</button>');  ?>
										</div>
									</div>
								<?php echo form_close(); ?>

							</div>
						</div>
					</div>
        
				</div>
		  
		  
			</div>
        <!-- /.box-body -->
	
	

              <!--end of tambahan panel table-->

			</section>