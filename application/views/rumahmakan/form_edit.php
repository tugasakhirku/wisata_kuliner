<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
              <!--overview start-->
			  <div class="row">
				<div class="col-lg-12">
					<!-- <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3> -->
                    <!-- <h4 class="page-header"><i class=""></i> Dashboard</h3> -->

					<ol class="breadcrumb">
						<li><i class="page-header"></i><a href="indexis.html">Home</a></li>
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Profil</a></li> -->
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Peta</a></li> -->
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Daftar Tempat Kuliner</a></li> -->
						<li><i class="fa fa-home"></i>Rumah Makan</li>
					</ol>
				</div>
			  </div>
              <!--end of overview start-->

              <!-- /.panel-heading -->
                <div class="panel-body">



                </div>
              <!-- /end of.panel-heading -->

              <!--tambahan panel table-->
			<div class="box-body">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div id="header">
								<h1 id="logo">Tambah Rumah Makan</h1>
								<hr>
							<div class="widget-body">
								<?php echo form_open_multipart('rumahmakan/proses_edit', 'class="form-horizontal", row-border'); ?>
									<div class="form-group">
										<label class="col-md-2 control-label">Id Rumah Makan</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="id_rumah_makan" value="<?php echo $entry->id_rumah_makan ?>"  readonly>
											</div>
									</div>

									<!-- <div class="form-group">
										<label class="col-md-2 control-label">Id Tempat Makan</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="id_barang" placeholder="" maxlength="100"  readonly>
											</div>
									</div> -->

									<div class="form-group">
										<label class="col-md-2 control-label">Nama Rumah Makan / Restoran</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="nama_rumah_makan" value="<?php echo $entry->nama_rumah_makan ?>" maxlength="100" >
											</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Alamat Rumah Makan</label>
											<div class="col-md-6">
												<textarea class="form-control" rows="3" name="alamat_rumah_makan"  ><?php echo $entry->alamat_rumah_makan ?></textarea>
											</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">No Hp Rumah Makan</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="nomor_hp_rumah_makan" value="<?php echo $entry->nomor_hp_rumah_makan ?>" maxlength="100"  >
											</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Kecamatan</label>
											<div class="col-md-6">
												<select name="id_kecamatan" class="form-control" style="width: 450px">
													<?php
														foreach($k as $f){
															if($entry->id_kecamatan == $f->id_kecamatan){
															echo "<option value='$f->id_kecamatan' selected>$f->nama_kecamatan</option>";
														}else{
															echo "<option value='$f->id_kecamatan' >$f->nama_kecamatan</option>";
														}
														}
													?>
												</select>
											</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Deskripsi</label>
											<div class="col-md-6">
												<textarea class="form-control" rows="3" name="deskripsi_rumah_makan" ><?php echo $entry->deskripsi_rumah_makan ?></textarea>
											</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Latitude</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="latitude" value="<?php echo $entry->latitude ?>" maxlength="100" >
											</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Longitude</label>
											<div class="col-md-6">
												<input class="form-control" type="text" name="longitude" value="<?php echo $entry->longitude ?>" maxlength="100" >
											</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label">Gambar</label>
											<div class="col-md-6">
												<img id="slider" src="<?php echo base_url().'files/rumahmakan/'.$entry->gambar;?>" alt="..." width="100px" height="100px">
												<br>
												<input class="form-control" type="file" name="userfile"  >

											</div>
									</div>


									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<button type="submit" class="btn btn-info" >Simpan</button>
												<?php echo anchor('/rumahmakan', '<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="kembali ke file rumah makan">Kembali</button>');  ?>
										</div>
									</div>
								<?php echo form_close(); ?>

							</div>
						</div>
					</div>

				</div>


			</div>
        <!-- /.box-body -->



              <!--end of tambahan panel table-->

			</section>
