<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Wisata Kuliner Kota Kendari</title>

    <!-- Bootstrap CSS -->        
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	
    <!-- bootstrap theme -->    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css">
	
    <!--external css-->
    <!-- font icon -->    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/elegant-icons-style.css">    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
	
    <!-- full calendar css-->    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/fullcalendar/fullcalendar/fullcalendar.css">	
	
    <!-- easy pie chart-->    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css">	
    
	<!-- owl carousel -->    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-jvectormap-1.2.2.css">	
	
	
    <!-- Custom styles -->	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/widgets.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style-responsive.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style-responsive.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/xcharts.min.css">	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.4.min.css">	
	
    <!-- =======================================================
        Theme Name: NiceAdmin
        Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
        Author: BootstrapMade
        Author URL: https://bootstrapmade.com
    ======================================================= -->
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      <!--header -->
      <header class="header dark-bg">
           <!--  <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div> -->

            <!--logo start-->
            <a href="index.html" class="logo">Sistem Informasi <span class="lite">Kuliner Kota Kendari</span></a>
            <!--logo end-->

            <div class="nav search-row" id="top_menu">
                <!--  search form start -->
                <!-- <ul class="nav top-menu">                    
                    <li>
                        <form class="navbar-form">
                            <input class="form-control" placeholder="Search" type="text">
                        </form>
                    </li>                    
                </ul> -->
                <!--  search form end -->                
            </div>

            <div class="top-nav notification-row">                
                <!-- notificatoin dropdown start-->
                <ul class="nav pull-right top-menu">
                    

                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <span class="profile-ava">
                                <img alt="" src="img/avatar1_small.jpg">
                                <link alt="" href="<?php echo base_url(); ?>assets/img/icons/avatar1_small.jpg"> 
                            </span> -->
                            <span class="username">Admin</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="#"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_mail_alt"></i> My Inbox</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_clock_alt"></i> Timeline</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_chat_alt"></i> Chats</a>
                            </li>
                            <li>
                                <a href="login.html"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                            <li>
                                <a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
                            </li>
                            <li>
                                <a href="documentation.html"><i class="icon_key_alt"></i> Documentation</a>
                            </li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->