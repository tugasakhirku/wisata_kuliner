<!--main content start-->
		<section id="main-content">
			<section class="wrapper">            
              <!--overview start-->
			  <div class="row">
				<div class="col-lg-12">
					<!-- <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3> -->
                    <!-- <h4 class="page-header"><i class=""></i> Dashboard</h3> -->

					<ol class="breadcrumb">
						<li><i class="page-header"></i><a href="#">Home</a></li>
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Profil</a></li> -->
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Peta</a></li> -->
                        <!-- <li><i class="fa fa-home"></i><a href="indexis.html">Daftar Tempat Kuliner</a></li> -->
						<li><i class="fa fa-home"></i>Rumah Makan</li>						  	
					</ol>
				</div>
			  </div>
              <!--end of overview start-->

              <!-- /.panel-heading -->
                <div class="panel-body">
                    
                <form class="navbar-form navbar-left" role="search">
                    <div class="btn-group">
                        <?php echo anchor('rumahmakan/add', 
                                '<button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Tambah Data Pegawai"><i class="fa fa-plus"></i> Tambah Rumah Makan</button>' );?>
                                &nbsp;
                                <?php echo anchor('Rumahmakan', 
                                '<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Refresh"><i class="fa fa-refresh"></i> Refresh </button>' );?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    
                    <!-- <button type="submit" class="btn btn-default" title="Cari Data" >Go</button> -->
                    <div class="btn-group">
                        <input type="submit" name="submit" class="btn btn-info" title="Cari Data" value="Go">
                    </div>                    
                  
                </form>

                </div>
              <!-- /end of.panel-heading -->

              <!--tambahan panel table-->
    <div class="panel-body">
        <div class="dataTable_wrapper">
            <div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
				<thead>
				<tr>
					<th style="width:5%"><center>No</th>
					<th style="width:10%"><center>Nama</th>                
					<th style="width:15%"><center>Deskripsi</th>
					<th style="width:10%"><center>Kecamatan</th>
					<th style="width:10%"><center>Latitude</th>
					<th style="width:10%"><center>Longitude</th>
					<th style="width:10%"><center>Gambar</th>
					<th style="width:20%"><center>Pilihan</th>
				</tr>
				</thead>
				<tbody>
			   
					<tr class="gradeX" >
					
					</tr>
				
				</tbody>
				</table>
            </div>
            <!-- <div align="right"><?php echo $links?> </div> -->
        </div>
    </div>

              <!--end of tambahan panel table-->

			</section>