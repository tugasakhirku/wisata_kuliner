-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2017 at 04:17 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wisata_kuliner`
--

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas`
--

CREATE TABLE IF NOT EXISTS `fasilitas` (
  `id_fasilitas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_fasilitas` varchar(200) NOT NULL,
  PRIMARY KEY (`id_fasilitas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE IF NOT EXISTS `kecamatan` (
  `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_kecamatan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `nama_kecamatan`) VALUES
(1, 'abeli'),
(2, 'pura pura');

-- --------------------------------------------------------

--
-- Table structure for table `rumah_makan_tbl`
--

CREATE TABLE IF NOT EXISTS `rumah_makan_tbl` (
  `id_rumah_makan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_rumah_makan` varchar(100) NOT NULL,
  `alamat_rumah_makan` varchar(500) NOT NULL,
  `nomor_hp_rumah_makan` varchar(12) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `deskripsi_rumah_makan` varchar(500) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_rumah_makan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rumah_makan_tbl`
--

INSERT INTO `rumah_makan_tbl` (`id_rumah_makan`, `nama_rumah_makan`, `alamat_rumah_makan`, `nomor_hp_rumah_makan`, `id_kecamatan`, `deskripsi_rumah_makan`, `latitude`, `longitude`, `gambar`) VALUES
(1, 'qwerty', 'qwerty', '123568997665', 1, 'qwerty', 1234567890, 1234567890, 'qwerty'),
(2, 'qwerty', 'qwerty', '123568997665', 1, 'qwerty', 32.30642, -122.61458, 'qwerty'),
(3, 'qwerty', 'qwerty', '12356899766', 1, 'qwerty', 1234567890, 1234567890, 'qwerty');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
